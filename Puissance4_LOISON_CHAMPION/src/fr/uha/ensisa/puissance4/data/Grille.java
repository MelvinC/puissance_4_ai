package fr.uha.ensisa.puissance4.data;

import fr.uha.ensisa.puissance4.util.Constantes;
import fr.uha.ensisa.puissance4.util.Constantes.Case;

public class Grille {

	private Case[][] grille;

	public Grille() {
		grille = new Case[Constantes.NB_COLONNES][Constantes.NB_LIGNES];
		for (int i = 0; i < Constantes.NB_COLONNES; i++)
			for (int j = 0; j < Constantes.NB_LIGNES; j++) {
				grille[i][j] = Case.V;
			}
	}

	/**
	 * Constructeur permettant de créer une copie de la grille donnée en argument
	 * 
	 * @param original
	 */
	private Grille(Grille original) {
		grille = new Case[Constantes.NB_COLONNES][Constantes.NB_LIGNES];
		for (int i = 0; i < Constantes.NB_COLONNES; i++)
			for (int j = 0; j < Constantes.NB_LIGNES; j++) {
				grille[i][j] = original.getCase(j, i);
			}
	}

	/**
	 * Renvoie le contenu de la case aux coordonnées données en argument
	 * 
	 * @param ligne
	 * @param colonne
	 * @return
	 */
	public Case getCase(int ligne, int colonne) {
		// System.out.println("ligne : " + ligne + " colonne : " + colonne);
		return grille[colonne][ligne];
	}

	/**
	 * Indique s'il y a encore de la place dans la colonne indiquée
	 * 
	 * @param colonne
	 * @return
	 */
	public boolean isCoupPossible(int colonne) {
		if (colonne >= 0 && colonne < Constantes.NB_COLONNES) {
			return grille[colonne][Constantes.NB_LIGNES - 1] == Case.V;
		} else {
			return false;
		}
	}

	/**
	 * Ajoute le symbole indiqué dans la colonne indiquée ce qui permet de jouer
	 * ce coup
	 * 
	 * @param colonne
	 * @param symbole
	 */
	public void ajouterCoup(int colonne, Case symbole) {
		for (int j = 0; j < Constantes.NB_LIGNES; j++) {
			if (grille[colonne][j] == Case.V) {
				grille[colonne][j] = symbole;
				break;
			}
		}

	}

	/**
	 * Renvoie l'état de la partie
	 * 
	 * @param symboleJoueurCourant
	 * @param tour
	 * @return
	 */
	public int getEtatPartie(Case symboleJoueurCourant, int tour) {
		int victoire;
		if (symboleJoueurCourant == Constantes.SYMBOLE_J1) {
			victoire = Constantes.VICTOIRE_JOUEUR_1;
		} else {
			victoire = Constantes.VICTOIRE_JOUEUR_2;
		}
		int nbAlignes = 0;
		// Vérification alignement horizontaux
		for (int i = 0; i < Constantes.NB_LIGNES; i++) {
			for (int j = 0; j < Constantes.NB_COLONNES; j++) {
				if (grille[j][i] == symboleJoueurCourant)
					nbAlignes++;
				else
					nbAlignes = 0;
				if (nbAlignes == 4) {
					return victoire;
				}
			}
			nbAlignes = 0;
		}
		// Vérification alignement verticaux
		for (int j = 0; j < Constantes.NB_COLONNES; j++) {
			for (int i = 0; i < Constantes.NB_LIGNES; i++) {
				if (grille[j][i] == symboleJoueurCourant)
					nbAlignes++;
				else
					nbAlignes = 0;
				if (nbAlignes == 4) {
					return victoire;
				}
			}
			nbAlignes = 0;
		}
		// Vérification alignement diagonaux (bas-droite vers haut-gauche)
		for (int i = 0; i < Constantes.NB_LIGNES - 3; i++)
			for (int j = 0; j < Constantes.NB_COLONNES - 3; j++) {
				for (int x = 0; i + x < Constantes.NB_LIGNES && j + x < Constantes.NB_COLONNES; x++) {
					if (grille[j + x][i + x] == symboleJoueurCourant)
						nbAlignes++;
					else
						nbAlignes = 0;
					if (nbAlignes == 4) {
						return victoire;
					}
				}
				nbAlignes = 0;
			}

		// Vérification alignement diagonaux (bas-gauche vers haut-droit)
		for (int i = 0; i < Constantes.NB_LIGNES - 3; i++)
			for (int j = Constantes.NB_COLONNES - 1; j >= 3; j--) {
				for (int x = 0; i + x < Constantes.NB_LIGNES && j - x >= 0; x++) {
					if (grille[j - x][i + x] == symboleJoueurCourant)
						nbAlignes++;
					else
						nbAlignes = 0;
					if (nbAlignes == 4) {
						return victoire;
					}
				}
				nbAlignes = 0;
			}

		if (tour == Constantes.NB_TOUR_MAX) {
			return Constantes.MATCH_NUL;
		}

		return Constantes.PARTIE_EN_COURS;
	}

	/**
	 * Donne un score à la grille en fonction du joueur
	 * 
	 * @param symboleJoueurCourant
	 * @return
	 */
	public double evaluer(Case symboleJoueurCourant) {
		double score = 0;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			for (int j = Constantes.NB_LIGNES - 1; j >= 0; j--) {
				if (j - 3 >= 0) {
					Case c1 = grille[i][j];
					Case c2 = grille[i][j - 1];
					Case c3 = grille[i][j - 2];
					Case c4 = grille[i][j - 3];
					score += scoring(c1, c2, c3, c4, symboleJoueurCourant);
				}
				if (i + 3 < Constantes.NB_COLONNES) {
					Case c1 = grille[i][j];
					Case c2 = grille[i + 1][j];
					Case c3 = grille[i + 2][j];
					Case c4 = grille[i + 3][j];
					score += scoring(c1, c2, c3, c4, symboleJoueurCourant);
				}
				if (i + 3 < Constantes.NB_COLONNES && j - 3 >= 0) {
					Case c1 = grille[i][j];
					Case c2 = grille[i + 1][j - 1];
					Case c3 = grille[i + 2][j - 2];
					Case c4 = grille[i + 3][j - 3];
					score += scoring(c1, c2, c3, c4, symboleJoueurCourant);
				}
				if (i - 3 >= 0 && j - 3 >= 0) {
					Case c1 = grille[i][j];
					Case c2 = grille[i - 1][j - 1];
					Case c3 = grille[i - 2][j - 2];
					Case c4 = grille[i - 3][j - 3];
					score += scoring(c1, c2, c3, c4, symboleJoueurCourant);
				}
			}
		}
		return score;
	}

	private double scoring(Case c1, Case c2, Case c3, Case c4, Case symboleJoueurCourant) {
		double score = 0;
		int num = 0;
		Case adversaire = null;
		if (symboleJoueurCourant.equals(Case.O))
			adversaire = Case.X;
		else
			adversaire = Case.O;
		if (c1.equals(symboleJoueurCourant))
			num++;
		if (c2.equals(symboleJoueurCourant))
			num++;
		if (c3.equals(symboleJoueurCourant))
			num++;
		if (c4.equals(symboleJoueurCourant))
			num++;
		if (c1.equals(adversaire) || c2.equals(adversaire) || c3.equals(adversaire) || c4.equals(adversaire))
			num = 0;
		if (!c1.equals(symboleJoueurCourant) && !c2.equals(symboleJoueurCourant) && !c3.equals(symboleJoueurCourant)
				&& !c4.equals(symboleJoueurCourant)) {
			if (c1.equals(adversaire))
				num--;
			if (c2.equals(adversaire))
				num--;
			if (c3.equals(adversaire))
				num--;
			if (c4.equals(adversaire))
				num--;
		}
		switch (num) {
		case 0:
			score += 0;
			break;
		case 1:
			score += 1;
			break;
		case 2:
			score += 10;
			break;
		case 3:
			score += 1000;
			break;
		case 4:
			score += 1000000;
			break;
		case -1:
			score += -1;
			break;
		case -2:
			score += -10;
			break;
		case -3:
			score += -1000;
			break;
		case -4:
			score += -1000000;
			break;
		default:
			score += 0;
			break;
		}
		return score;
	}

	/**
	 * Clone la grille
	 */
	public Grille clone() {
		Grille copy = new Grille(this);
		return copy;
	}

}
