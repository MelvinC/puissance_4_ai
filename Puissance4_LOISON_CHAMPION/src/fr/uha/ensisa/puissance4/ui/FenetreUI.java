package fr.uha.ensisa.puissance4.ui;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class FenetreUI extends JFrame{
	
	private GrilleUI grille;
	private Console console;
	
	public FenetreUI(Console c){             
	    this.setTitle("Puissance 4");
	    this.setSize(1500, 1000);
	    this.setLocationRelativeTo(null);               
	    this.grille = new GrilleUI(c);
	    //grille.setBackground(Color.GRAY);        
	    this.setContentPane(grille);               
	    this.setVisible(true);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.console=c;

	}
	
	public GrilleUI getGrilleUI() {
		return this.grille;
	}
	
	public Console getConsole() {
		return this.console;
	}
}
