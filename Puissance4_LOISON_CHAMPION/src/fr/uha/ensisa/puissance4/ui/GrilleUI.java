package fr.uha.ensisa.puissance4.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JPanel;

import fr.uha.ensisa.puissance4.data.Grille;
import fr.uha.ensisa.puissance4.data.IA;
import fr.uha.ensisa.puissance4.data.Partie;
import fr.uha.ensisa.puissance4.util.Constantes;
import fr.uha.ensisa.puissance4.util.Constantes.Case;

public class GrilleUI extends JPanel{
	
	private Grille grille;
	private Partie partie;
	private Console console;
	private int statutPartie;
	private Bouton bt1;
	private Bouton bt2;
	private Bouton bt3;
	private Bouton bt4;
	private Bouton bt5;
	private Bouton bt6;
	private Bouton bt7;
	
	public GrilleUI(Console c) {
		super();
		this.console=c;
        this.setLayout(null);
		grille = new Grille();
		bt1 = new Bouton("v", console, 1);
        bt1.setBounds(170, 50, 40, 100);
        this.add(bt1);
        bt2 = new Bouton("v", console, 2);
        bt2.setBounds(280, 50, 40, 100);
        this.add(bt2);
        bt3 = new Bouton("v", console, 3);
        bt3.setBounds(390, 50, 40, 100);
        this.add(bt3);
        bt4 = new Bouton("v", console, 4);
        bt4.setBounds(500, 50, 40, 100);
        this.add(bt4);
        bt5 = new Bouton("v", console, 5);
        bt5.setBounds(610, 50, 40, 100);
        this.add(bt5);
        bt6 = new Bouton("v", console, 6);
        bt6.setBounds(720, 50, 40, 100);
        this.add(bt6);
        bt7 = new Bouton("v", console, 7);
        bt7.setBounds(830, 50, 40, 100);
        this.add(bt7);
        this.statutPartie = Constantes.DEBUT_PARTIE;
	}
	
	public void paintComponent(Graphics g){
		boolean b=true;
		if(this.statutPartie == Constantes.PARTIE_EN_COURS) {
			if(partie != null) drawInformations(g);
			b=true;
			drawJeu(g);
		}
		if(this.statutPartie == Constantes.DEBUT_PARTIE) {
			b=false;
			drawStart(g);
		}
		if(this.statutPartie == Constantes.FIN_PARTIE) {
			drawInformations(g);
			drawJeu(g);
			drawGagnant(g);
			b=false;
		}
		bt1.setVisible(b);
		bt2.setVisible(b);
		bt3.setVisible(b);
		bt4.setVisible(b);
		bt5.setVisible(b);
		bt6.setVisible(b);
		bt7.setVisible(b);
	 }
	
	public void drawJeu(Graphics g) {
		g.setColor(Color.BLUE);
	    g.fillRoundRect(100, 200, 840, 690, 10, 10);
	    g.setColor(Color.WHITE);
	    for(int i = 0; i<7; i++) {
	    	for(int j = 0; j<6; j++) {
	    		g.fillOval(150+i*110, 240+j*110, 80, 80);
	    	}
	    }
	    for(int i = 0; i<Constantes.NB_LIGNES; i++) {
	    	for(int j = 0; j<Constantes.NB_COLONNES;j++) {
	    		Case c = grille.getCase(i, j);
	    		switch(c) {
	    		case X:
	    			g.setColor(Color.YELLOW);
	    			g.fillOval(150+j*110, 240+(Constantes.NB_LIGNES-i-1)*110, 80, 80);
	    			break;
	    		case O:
	    			g.setColor(Color.RED);
	    			g.fillOval(150+j*110, 240+(Constantes.NB_LIGNES-i-1)*110, 80, 80);
	    			break;
	    		case V:
	    			g.setColor(Color.WHITE);
	    			g.fillOval(150+j*110, 240+(Constantes.NB_LIGNES-i-1)*110, 80, 80);
	    			break;
	    		}
	    	}
	    }
	}
	
	public void drawStart(Graphics g) {
		Font font = new Font("my font", Font.ROMAN_BASELINE, 64);
		g.setFont(font);
		g.drawString("Merci de rentrer vos informations", 50, 100);
		g.drawString("de partie dans la console", 150, 200);
	}
	
	public void drawInformations(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(1000, 200, 400, 500);
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(1001, 201, 399, 499);
		g.setColor(Color.BLACK);
		Font titre = new Font("Arial", Font.BOLD, 20);
	    g.setFont(titre);
		g.drawString("Informations", 1150, 220);
		Font infos = new Font("Arial", Font.PLAIN, 16);
	    g.setFont(infos);
	    if(partie.getJoueur1().getType() == Constantes.JOUEUR_HUMAN)
	    	g.drawString("Joueur 1 : "+ partie.getJoueur1().getTypeNom()+" -> "+partie.getJoueur1().getNom(), 1010, 250);
	    else
	    	g.drawString("Joueur 1 : "+ partie.getJoueur1().getTypeNom()+ " "+ Constantes.IA_ALGOS[((IA)partie.getJoueur1()).getAlgoIA()] +" level : "+ ((IA)partie.getJoueur1()).getLevel() +" -> "+partie.getJoueur1().getNom(), 1010, 250);
	    if(partie.getJoueur2().getType() == Constantes.JOUEUR_HUMAN)
	    	g.drawString("Joueur 2 : "+ partie.getJoueur2().getTypeNom()+" -> "+partie.getJoueur2().getNom(), 1010, 280);
	    else {
	    	g.drawString("Joueur 2 : "+ partie.getJoueur2().getTypeNom()+ " "+ Constantes.IA_ALGOS[((IA)(partie.getJoueur2())).getAlgoIA()] + " level : "+ ((IA)(partie.getJoueur2())).getLevel() +" -> "+partie.getJoueur2().getNom(), 1010, 280);
	    }
	    g.setFont(titre);
	    g.drawString("Au tour de : " + partie.getJoueurCourant().getNom(), 1050, 680);
	    Color c = Color.WHITE;
	    switch(partie.getJoueurCourant().getSymbole()) {
	    case X:
	    	c=Color.YELLOW;
	    	break;
	    case O:
	    	c=Color.RED;
	    	break;
	    }
	    g.setColor(c);
	    g.fillOval(1270, 655, 30, 30);
	}
	
	public void drawGagnant(Graphics g) {
		Font gagnant = new Font("Arial", Font.BOLD, 70);
		g.setColor(Color.BLACK);
	    g.setFont(gagnant);
	    String msg;
	    switch(partie.getEtatPartie())
		{
			case Constantes.VICTOIRE_JOUEUR_1 : 
				msg=partie.getJoueur1().getNom() + " a gagn�";
				break;
			case Constantes.VICTOIRE_JOUEUR_2 : 
				msg=partie.getJoueur2().getNom() + " a gagn�";
				break;
			default : 
				msg="MATCH NUL";
				break;
		}
	    g.drawString(msg, 250, 400);
	}
	
	public void setGrille(Grille grille) {
		this.grille=grille;
	}
	
	public void setPartie(Partie partie) {
		this.partie=partie;
	}
	
	public void setStatutPartie(int i) {
		this.statutPartie = i;
	}
}
