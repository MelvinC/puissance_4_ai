package fr.uha.ensisa.puissance4.ui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

public class Bouton extends JButton implements MouseListener {
	
	private Console console;
	private int value;
	
	public Bouton(String s, Console c, int v) {
		super(s);
		this.console=c;
		this.value=v;
		this.addMouseListener(this);
	}

	public void mouseClicked(MouseEvent arg0) {	
		console.setCoup(value);
		synchronized(console) {
			console.notifyAll();
		}
	}

	public void mouseEntered(MouseEvent arg0) {

	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {		
	}

	public void mouseReleased(MouseEvent arg0) {		
	}

}
