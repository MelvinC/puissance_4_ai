package fr.uha.ensisa.puissance4.util;

public class Noeud {
	private int index;
	private double score;
	
	public Noeud() {
		
	}

	public Noeud(double score, int index) {
		this.index = index;
		this.score = score;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
}
