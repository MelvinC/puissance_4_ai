package fr.uha.ensisa.puissance4.jeu.algosIA;

import fr.uha.ensisa.puissance4.data.Grille;
import fr.uha.ensisa.puissance4.data.Joueur;
import fr.uha.ensisa.puissance4.util.Constantes;

/**
	Classe implémentant l'algorithme Alpha-Beta
*/
public class AlphaBeta extends Algorithm {

	
	public AlphaBeta(int levelIA, Grille grilleDepart, Joueur joueurActuel, int tour) {
		super(levelIA, grilleDepart, joueurActuel, tour);

	}

	@Override
	public int choisirCoup() {
		double v = Double.NEGATIVE_INFINITY;
		double alpha = Double.NEGATIVE_INFINITY;
		double beta = Double.POSITIVE_INFINITY;
		int index = 0;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			Grille temp = grilleDepart.clone();
			if (temp.isCoupPossible(i)) {
				temp.ajouterCoup(i, symboleMax);
				Double min = minValue(temp, 1, alpha, beta);
				System.out.println(min);
				if (min > v) {
					v = min;
					index = i;
				}
			}
		}
		return index;
	}

	private double minValue(Grille g, int level, double alpha, double beta) {
		if (g.getEtatPartie(symboleMax, tourMax) == Constantes.VICTOIRE_JOUEUR_2 || level == levelIA) {
			return g.evaluer(symboleMax);
		}
		double v = Double.POSITIVE_INFINITY;
		level++;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			Grille temp = g.clone();
			temp.ajouterCoup(i, symboleMin);
			v = Math.min(v, maxValue(temp, level, alpha, beta));
			if(v<=alpha) return v;
			beta = Math.min(beta, v);
		}
		return v;
	}

	private double maxValue(Grille g, int level, double alpha, double beta) {
		if (g.getEtatPartie(symboleMin, tourMax) == Constantes.VICTOIRE_JOUEUR_1 || level == levelIA) {
			return g.evaluer(symboleMax);
		}
		double v = Double.NEGATIVE_INFINITY;
		level++;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			Grille temp = g.clone();
			temp.ajouterCoup(i, symboleMax);
			v = Math.max(v, minValue(temp, level, alpha, beta));
			if(v>=beta) return v;
			alpha = Math.max(alpha, v);
		}
		return v;
	}
	
	
}
