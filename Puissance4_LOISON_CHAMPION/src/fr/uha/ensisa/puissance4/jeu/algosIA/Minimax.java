package fr.uha.ensisa.puissance4.jeu.algosIA;

import java.util.Vector;

import fr.uha.ensisa.puissance4.data.Grille;
import fr.uha.ensisa.puissance4.data.Joueur;
import fr.uha.ensisa.puissance4.util.Constantes;
import fr.uha.ensisa.puissance4.util.Noeud;

/**
 * Classe implémentant l'algorithme Minimax
 */
public class Minimax extends Algorithm {

	public Minimax(int levelIA, Grille grilleDepart, Joueur joueurActuel, int tour) {
		super(levelIA, grilleDepart, joueurActuel, tour);

	}

	@Override
	public int choisirCoup() {
		// TODO Auto-generated method stub
		double v = Double.NEGATIVE_INFINITY;
		int index = 0;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			Grille temp = grilleDepart.clone();
			if (temp.isCoupPossible(i)) {
				temp.ajouterCoup(i, symboleMax);
				Double min = minValue(temp, 1);
				if (min > v) {
					v = min;
					index = i;
				}
			}
		}
		return index;
	}

	private double minValue(Grille g, int level) {
		if (g.getEtatPartie(symboleMax, tourMax) == Constantes.VICTOIRE_JOUEUR_2 || level == levelIA) {
			return g.evaluer(symboleMax);
		}
		double v = Double.POSITIVE_INFINITY;
		level++;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			Grille temp = g.clone();
			temp.ajouterCoup(i, symboleMin);
			v = Math.min(v, maxValue(temp, level));
		}
		return v;
	}

	private double maxValue(Grille g, int level) {
		if (g.getEtatPartie(symboleMin, tourMax) == Constantes.VICTOIRE_JOUEUR_1 || level == levelIA) {
			return g.evaluer(symboleMax);
		}
		double v = Double.NEGATIVE_INFINITY;
		level++;
		for (int i = 0; i < Constantes.NB_COLONNES; i++) {
			Grille temp = g.clone();
			temp.ajouterCoup(i, symboleMax);
			v = Math.max(v, minValue(temp, level));
		}
		return v;
	}
}
